---
layout: post
title: " Linux debugging: Dazed and confused, but trying to continue"
categories:
- Linux
tags: [Linux,kernel,debug]
---

I got this rather curious kernel debug message, I assumed in was caused by my laptop going into standby, with a mounted sshfs share (using FUSE).
But after reading the kernel source, it seems like it was caused by unplugging my Ethernet cable (with a mounted sshfs share(?))

It seems to be an 'nmi-error' found in the linux kernel at: 2.6.26-1-686/arch/x86/kernel/traps_32.c (Debian Lenny)

---

![Kernel messages](/static/images/2009/kernel-messages1.jpg "Kernel messages")

{% highlight c %}
static notrace __kprobes void mem_parity_error(unsigned char reason, struct pt_regs *regs)
{
  printk(KERN_EMERG
  "Uhhuh. NMI received for unknown reason %02x on CPU %d.\n",
  reason, smp_processor_id());

  printk(KERN_EMERG
  "You have some hardware problem, likely on the PCI bus.\n");

  #if defined(CONFIG_EDAC)
  if (edac_handler_set()) {
  edac_atomic_assert_error();
  return;
  }
  #endif

  if (panic_on_unrecovered_nmi)
    panic("NMI: Not continuing");

  printk(KERN_EMERG "Dazed and confused, but trying to continue\n");

  /* Clear and disable the memory parity error line. */
  clear_mem_error(reason);
}

{% endhighlight %}