---
layout: post
title: " Bash script: a simple select menu"
categories:
- Linux
tags: [Linux,Bash]
---

As I started using Linux a couple years ago, I noticed I was moving to the command-line for many common tasks. When I noticed I was typing the same commands many times a day, I decided to to write some bash scripts to speed up some of these tasks.

The first part of these scripts is a simple select menu:

It shows several options, you select an option by typing a letter between brackets.
Some options start an other bash script, other options start python scripts.

More code will be released later.
 - kdialog version, backup scripts, ...

{% highlight bash %}
#!/bin/bash
#
# Script to perform some common system operations
#
while :
do
    clear
    echo "************************"
    echo "* My tools *"
    echo "************************"
    echo "* [b] Backup *"
    echo "* [a] Attach sshfs *"
    echo "* [u] Unmount sshfs *"
    echo "* [l] Server Login ssh *"
    echo "* [t] Tunnels over ssh *"
    echo "* [x] X-forwarding ssh *"
    echo "* [y] YouTube download *"
    echo "* [g] Portfolio grabber*"
    echo "* [0] Exit/Stop *"
    echo "************************"
    echo -n "Enter your menu choice [a-0]: "
    read yourch
    case $yourch in
        b) $HOME/.scripts/tools/backup ;;
        a) $HOME/.scripts/tools/mount ;;
        u) $HOME/.scripts/tools/umount ;;
        l) $HOME/.scripts/tools/ssh ;;
        t) $HOME/.scripts/tools/tunnel ;;
        x) $HOME/.scripts/tools/xforward ;;
        k) kate $HOME/.scripts/scripts ;;
        g) $HOME/.scripts/tools/pf-grabber.sh ;;
        y) python $HOME/.scripts/tools/youtube.py ;;
        0) exit 0;;
        *) echo "Oopps!!! Please select choice 1,2,3 or 4";
        echo "Press Enter to continue. . ." ; read ;;
    esac
done
{% endhighlight %}