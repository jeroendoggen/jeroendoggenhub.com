---
layout: post
title: " Study of RSS-Based Localisation in Wireless Sensor Networks"
categories:
- conference
tags: [Wireless Sensor Network, received signal strength, localisation]
---

*Doggen J., Pauwels S., Van der Schueren F., Vaningelgem L., Schaeps T., Schrooyen F., Goossens M., Castreuil A., Pelkmans K., "Thematic Weeks: a New Concept in Engineering Education", ICEE Hungary, New Challenges in Engineering Education and Research in the 21st Century, Budapest, August 2008.*

[Full Text][paper], [Slides][presentation], [Google Scholar][scholar]

Localisation of nodes in wireless sensor networks (WSNs) is important to context-aware and position-dependent applications. 
Many algorithms exist for localising nodes using received signal strength. 
In this paper, we present a quantitative comparison of algorithms which use RSS as a ranging method and present a localisation software framework called Senseless. 
We conducted a survey about the influence of the orientation of a node. 
Our study finds that the received power is not equal in all directions and that no single best algorithm for localisation exists to date. 
Each algorithm has a different purpose and diverse properties. 
The optimal algorithm can be chosen for each environment.
In our environment, MinMax is the current algorithm of choice.
Using this framework, we implemented several centralised algorithms: Trilateration, Min-Max, Centroid Localisation and Weighted Centroid Localisation.

Keywords: Wireless Sensor Network, received signal strength, localisation.

[paper]: /static/articles/2010/ecumict2010.pdf
[presentation]: /static/articles/2010/ecumict2010pres.pdf
[scholar]: http://scholar.google.be/citations?view_op=view_citation&hl=en&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:d1gkVwhDpl0C
