---
layout: post
title: " Evaluation of NFC based ticketing system: 'Tapango'"
categories:
- conference
tags: [Wireless Sensor Network, received signal strength, localisation]
---

*Neefs J., Schrooyen F., Doggen J, and Renckens K.: “Paper ticketing vs. Electronic Ticketing based on off-line system ’Tapango’,” IEEE International Workshop on Near Field Communication, Monaco, april 2010.*

[Full Text][paper], [Website][website], [Google Scholar][scholar]

An electronic voucher system, which intended to replace paper vouchers by an electronic wristlet was developed by the e-lab, Artesis' research lab. 
This project has lead to the development of the Tapango system. 
The NFC technology has been used to create one, universal "wallet" for different events, the system is an attempt to replace paper ticketing services used nowadays. 
In this paper we provide a valid comparison between electronic and paper ticketing system by means of user feedback, benchmarking and real-life test cases.

Keywords: Electronic ticketing, Paper ticketing, NFC, Mifare, Tapango, Electronic wallet.

[paper]: /static/articles/2010/nfc2010.pdf
[website]: http://www.nfc-research.at/nfc2010/
[scholar]: http://scholar.google.be/citations?view_op=view_citation&hl=en&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:u-x6o8ySG0sC