---
layout: post
title: " LaTeX Beamer template: AP University College Antwerp"
categories:
- latex
tags: [latex, beamer, AP]
---

# LaTeX Beamer template: AP University College Antwerp

LaTeX Beamer template for AP University College Antwerpen (AP Hogeschool Antwerpen).

The template is loosely based on the official PowerPoint template.

[Code available on Github](https://github.com/jeroendoggen/AP-Latex-beamer-template)

## Example slides:

### A title page:

![A title page](https://raw.github.com/jeroendoggen/AP-Latex-beamer-template/master/screenshots/slide1.png?raw=true)

### An outline slide:

![An oultline slide](https://raw.github.com/jeroendoggen/AP-Latex-beamer-template/master/screenshots/slide2.png?raw=true)

### A slide with some math:

![A math slide](https://raw.github.com/jeroendoggen/AP-Latex-beamer-template/master/screenshots/slide3.png?raw=true)

