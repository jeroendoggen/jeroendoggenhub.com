---
layout: post
title: "EPS Providers meeting, FH St. Polten, Austria"
categories:
- meetings
tags: [talks, EPS, provider meeting, Autria, St. Polten, Universiteit Antwerpen, European Project Semester]
---

I visited St. Polten University of Applied sciences in Austria to attend the yearly European Project Semester (EPS) providers meeting.

At this meeting all 14 EPS partners present their latest news on EPS: finished projects, current projects, policy changes, new courses,...

An other goal of these meetings is to tighten the network and prepare future student and teacher exchange.

The following partners showed some interesting projects for our own "electronics-ICT" students.
 * [Lodz, Poland: Smart cities][eps_lodz]
 * [Tarbes, Frankrijk:  Autonomous Robotic Weed Control Systems][eps_tarbes]
 * [Porto, Portugal: various projects (but not a direct link...)][eps_porto]

Our Erasmus partner from St. Polten will be starting their EPS program in the next semester.
I hope that I will be able to help Thiemo & Jacob to define nice EPS projects at their side so our students can visit St. Polten for an EPS in the near future.

[These][presentation] are the slides of my presentation on the EPS news in Antwerp.

Some links:
 * [General EPS website][eps_site]
 * [EPS in Antwerp blog][eps_site_antwerp]
 * [EPS page Artesis Plantijn University college][eps_site_ap]
 * [EPS page University of Antwerp][eps_site_ua]

[presentation]: /static/presentations/2013/EPS_ProviderMeeting_Austria_2013.pdf

[eps_site]: http://www.europeanprojectsemester.eu/
[eps_site_antwerp]: http://epsantwerp.wordpress.com/
[eps_site_ap]: http://www.ap.be/node/942
[eps_site_ua]: https://www.uantwerpen.be/nl/faculteiten/ontwerpwetenschappen/studeren-en-onderwijs/internationaal/

[eps_lodz]: http://www.ife.p.lodz.pl/index.php?option=com_content&view=article&id=3326&Itemid=451&lang=en
[eps_tarbes]: http://www.enit.fr/fr/affaires-internationales/studying-at-enit/application-forms/eps.html
[eps_porto]: http://www.isep.ipp.pt/
