---
layout: post
title: " Book chapter: 'De weg naar praktijkgericht onderzoek'"
categories:
- books
tags: [onderzoek, publicaties, praktijkgericht, pba, Tim Dams]
---

Book chapter: "praktijkgericht onderzoek"

[Publisher page][asp]

My colleague Tim Dams and I wrote a short chapter in the Dutch book "Praktijkgericht wetenschappelijk onderzoek. Praktijken & perspectieven".

This book gives an overview of the practice oriented research and development activities at Artesis University College.

The book was written by Tine Rams and Koen Rymenants just before Artesis University College and Plantijn University College merged to become Artesis Plantijn University College.

In our chapter we describe our first steps in research and development and the problems we faced while defining our targets.

Dutch summary:

*Praktijkgericht wetenschappelijk onderzoek. Praktijken & perspectieven biedt een staalkaart van de expertise die binnen de professionele bacheloropleidingen van de Artesis Hogeschool Antwerpen is ontwikkeld. Het is een veelkleurig palet: van gezondheidszorg, technologie en bedrijfskunde tot sociaal werk, onderwijs en podiumkunsten. Daarnaast doen onderzoekers uit al deze domeinen verslag van concrete projecten uit heden en verleden. Zo vormt deze bundel ook een kleine geschiedenis van ruim tien jaar praktijkgericht onderzoek. Tot slot maken vier ervaringsdeskundigen de balans op: waar staat het praktijkgericht onderzoek vandaag en welke uitdagingen staan ons te wachten?*

*Lectoren, onderzoekers en beleidsmedewerkers vinden in dit boek inspirerende voorbeelden voor hun dagelijkse praktijk. Het werkveld krijgt een handig overzicht van de kennis en de samenwerkingsmogelijkheden die de hogeschool biedt. Praktijkgericht wetenschappelijk onderzoek. Praktijken & perspectieven wil voor iedereen die betrokken is bij AP, Artesis Plantijn Hogeschool Antwerpen, een aansporing zijn om enthousiast mee te denken over het praktijkgericht onderzoek in de nieuwe hogeschool.*

[asp]: http://www.aspeditions.be/article.aspx?article_id=PRAKTI917M
[blackboard]: http://www.blackboard.com/