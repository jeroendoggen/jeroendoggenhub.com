---
layout: post
title: " Genetic programming: Playing with generative algorithms"
categories:
- development
tags: [c++,genetic,programming,evolution]
---

I recently found a small software project while cleaning up some old folders on my PC.
The program uses the concept of evolution from biology to solve simple “problems”.

I wrote this code while reading the book The Selfish Gene by Richard Dawkins.
Somewhere in that book, the author ask the reader to write their own simulator and I was crazy enough to actually do that...

It was after programming this application and reading several of his books that I decided to start my studies in environmental sciences at the open university in the Netherlands.

Program flow:
 1. Enter a 'secret key'
 2. Generate a random string (parent)
 3. Generate 1000 strings based on the parent (with minor mutations)
 4. Select the child that is closest to the 'secret key' (lowest 'Deltasum' or the 'fittest')
 5. This child becomes the parent for the next generation
 6. Go back to step 3 until the secret key is found

 Code available on [Github](https://github.com/jeroendoggen/GenerativeAlgorithms)