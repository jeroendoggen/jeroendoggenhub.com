---
layout: post
title: " Poster: Arduino Cheat Sheet"
categories:
- latex
tags: [LaTeX, Arduino, poster]
---

[Github page][github]

# Arduino Cheat Sheet
"Arduino Cheat Sheet" is a poster that gives an overview of the most important Arduino commands and syntax.

It is based on [ArduinoCheatSheet](https://sites.google.com/site/mechatronicsguy/arduinocheatsheet "ArduinoCheatSheet") by The Mechatronics Guy.

The poster is created with Brian Amberg's [baposter LaTeX poster style](http://www.brian-amberg.de/uni/poster/ "baposter LaTeX poster style").

All the information is based on the [Arduino Language Reference website.](http://arduino.cc/en/Reference/HomePage "Arduino Language Reference website").

## Example:
Download [.pdf version](https://github.com/jeroendoggen/Arduino-cheat-sheet/blob/master/src/Arduino-Cheat-Sheet.pdf?raw=true)

### Complete poster (lowres):

![Complete poster (lowres)](https://raw.github.com/jeroendoggen/Arduino-cheat-sheet/master/examples/Arduino-Cheat-Sheet_v0.1.png?raw=true)

[github]: https://github.com/jeroendoggen/Arduino-serial-messaging