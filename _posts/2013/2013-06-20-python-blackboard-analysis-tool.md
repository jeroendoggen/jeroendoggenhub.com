---
layout: post
title: " Blackboard Analysis Tool"
categories:
- python
tags: [Python, Blackboard, student, grading, reports, automation]
---

[Github page][github], [blackboard.com][blackboard]

An analysis automation tool to avoid repetitive task while grading student assignments that have been handed in through the Blackboard learning system.

For example:
 * Extract hundreds of .zip files
 * Sort the files per assignment, student
 * Create statistics: number of students, late assignments,...

## Installation:
 * Download the source and run ``python setup.py install``.
 * Python Package available in the Python Package Index at: http://pypi.python.org/pypi/blackboard_analysis_tools/.
 * Install using pip: ``pip install blackboard_analysis_tools``.
 
## Usage:
 * Download assignment files from Blackboard (e.g. gradebook_ART_EA-38302_Assignment1_2013-06-11-20-50-44.zip).
 * Place these .zip files in the "input" folder.
 * Run the program: ``python -m blackboard_analysis_tools``.
 * Wait... (about 10 seconds when processing around 100MB of assignments) (time will vary).
 * Open the "output" folder to see the results (all files sorted per student, a summary, logfile, ...).

## What is happening behind the scenes:
 1. Scan for .zip files
 2. Extract the .zip files
 3. Scan for .txt files (these contain the metadata that describes the student reports/assignments)
 4. Analyse the .txt files
 5. Create a folder for each student
 6. Move all the files to the correct folder (including those with filenames that have been 'mangled' by Blackboard)
 7. Write some statistics: a list of all students that have handed something in
 8. Write a summary of the process: number of students, number of assignments, number of 'mangled' files,...

[github]: https://github.com/jeroendoggen/blackboard-analysis-tools
[blackboard]: http://www.blackboard.com/