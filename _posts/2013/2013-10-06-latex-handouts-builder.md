---
layout: post
title: " LaTeX Handouts Builder"
categories:
- latex
tags: [Python, LaTeX, handouts, automation, course]
---

[Github page][github]

Build Script for Latex-beamer based course handouts.
This tool can be used to build "course handout books" using multiple LaTeX beamer slides sets.

### What is it does:
 * Build multiple LaTeX beamer slide sets with one command: ``python build.py``
 * Convert the slides to a printer-friendly format (no slide transitions, 6 slides per page, less colors)
 * Build a main "course handouts book" with all the slides (one chapter per beamer slide set, title page, introduction, table of contents,...)
 * Create a .zip archive with all the documents

### Book structure:
Title page, introduction, table of contents

One chapter per slide set (three printer-friendly versions: no slides transitions &  less colors)
 * three slides per page with room for notes (example)
 * six slides per page (example)
 * two slides per page (example)

[github]: https://github.com/jeroendoggen/latex-handouts-builder
