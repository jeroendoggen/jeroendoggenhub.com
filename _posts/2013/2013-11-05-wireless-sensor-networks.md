---
layout: post
title: " Wireless Sensor Networks, University of Antwerp"
categories:
- talks
tags: [talks,wireless sensor networks, Maarten Weyn, Universiteit Antwerpen, University of Antwerp]
---

Maarten Weyn invited me at the University of Antwerp to his class to give a guest lecture on Wireless Sensor Networks.
I was happy to see some of my former students in the audience.

Main topics of the talk:
 * history of WSNs
 * typical hardware platforms and design issues
 * Arduino-based WSN application example

[Download the slides][presentation]

[presentation]: /static/presentations/2013/WirelessSensorNetworks_pres.pdf
[ambient2013_paper]: jeroendoggen.github.io....