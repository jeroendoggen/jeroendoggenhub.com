---
layout: post
title: " Serial Messaging Protocol"
categories:
- arduino
tags: [Arduino]
---

[Github page][github], [Arduino forum thread][forum]

## Scratching your own itch...
As a university lecturer, I was annoyed by the fact that, every year, all my students were implementing their own very simple protocols to connect Arduino & pc applications.
As a results it was hard to reuse their code and most of the time it is hard to interconnect applications from different teams.

## Why this can be useful
This library was developed to allow multiple teams of students to work together on Arduino/embedded Linux projects
Some teams working on the embedded Arduino side, other teams working on Python applications running on the Raspberry PI.
The goal is to have a standardised "communication protocol" to ensure interoperability between software/hardware blocks.
Hopefully this will allow future student to start from the current code base to develop their projects.

## The concept
We are developing a library  to exchange short messages (sensordata, commands) between an Arduino and software applications running on a PC. (Linux, embedded Linux, Windows, OS X)

Both sides are able send and receive a range of "standardised" messages over the serial port. All communication is done by sending short human readable ASCII messages. We define several standard command and data packet IDs to provide interoperability between different applications.

The protocol was designed to be relatively easy to comprehend and process by both human and computer:
 * A standardised message structure (more info below)
 * All fields in a message are separated by delimiters (just a letter indicating what the next field will be)
 * All communication is in HEX ASCII values (to allow human and computer to understand the packets)
 * Invalid messages are detected by calculating the parity (XOR all serial bytes)

## The Message Types
The library support seven basic packet types.
 * "Command": start motor, blink LED, go to sleep mode,...
 * "Command Reply": e.g. motor started, LED blinking, going to sleep mode now,...
 * "8-bit Data": (standard Arduino Byte type): e.g. temperature is 25 C, distance is 50 cm, humidity is 56%
 * "16-bit Data": (standard Arduino int type): e.g. temperature is -25 C, distance is 310 cm, time is 16200 seconds,...
 * "Data array": send multiple sensor measurements in one burst (useful when sampling very rapidly)
 * "Data request": e.g. send me the temperature, distance, humidity, ...
 * "Data array request": e.g. send me a burst of measurements
 
## Example Messages

Command message **T01N00I12PFFQ21** : "set motor speed of 'Arduino zero' to +100%:

 * **T01**: Type      01: Command message
 * **N00**: Number    00: Node number 00 (is the destination)
 * **I12**: CommandID 12: Set motor speed
 * **PFF**: Payload   FF: full speed (range: 0 (reverse) -> 80 (stopped) -> FF (forward))
 * **Q21**: Quality   21: parity byte is 21

Data message **T12N00I10P08Q0A** : "temperature of 'Arduino zero' is 8 degrees"

 * **T12**: Type      12: Data message (1 byte payload)
 * **N00**: Number    00: Node number 00 (is the source)
 * **I10**: SensorID  10: Temperature
 * **P08**: Payload   08: 8 degrees
 * **Q0A**: Quality   0A: parity byte is 0A

[github]: https://github.com/jeroendoggen/Arduino-serial-messaging
[forum]: http://forum.arduino.cc/index.php?topic=195224