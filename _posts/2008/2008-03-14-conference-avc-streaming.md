---
layout: post
title: " Design and Simulation of a H.264 AVC Video Streaming Model"
categories:
- conference
tags: [H.264 AVC, video streaming, simulation, OPNET Modeler, Filip Van der Schueren]
---

*Doggen J, Van der Schueren F., "Design and Simulation of a H.264 AVC Video Streaming Model," , ECUMICT, European Conference on the Use of Modern Information and Communication Technologies, Ghent, March 2008*

[Full Text][paper], [Slides][presentation], [Google Scholar][scholar]

This paper explains the design process of a traffic simulation model for H.264 AVC video streaming. 
The simulation model for H.264 video streaming was developed using OPNET Modeler, an advanced network modeling and simulation tool. 
The gamma distributions used in the model are based on empiric mean and variance values. 
The stream generator was used in several simulation scenarios in conjunction with Ethernet and wireless LAN node and network models. 
The simulation results show that based on the high level characteristics in the time domain, a H.264 stream is very similar to a MPEG2 stream. 
Under stressed network conditions, the more advanced H.264 standard shows better results: lower queuing delays and less packet-loss.

Keywords: H.264 AVC, video streaming, simulation, OPNET Modeler



[paper]: /static/articles/2008/ecumict2008.pdf
[presentation]: /static/articles/2008/ecumict2008pres.pdf
[scholar]: http://scholar.google.be/citations?view_op=view_citation&hl=en&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:W7OEmFMy1HYC

