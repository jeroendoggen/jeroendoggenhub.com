---
layout: post
title: " Thematic Weeks: a New Concept in Engineering Education"
categories:
- conference
tags: [Thematic weeks, current research, week-long project, managerial skills, student involvement]
---

*Doggen J., Pauwels S., Van der Schueren F., Vaningelgem L., Schaeps T., Schrooyen F., Goossens M., Castreuil A., Pelkmans K., "Thematic Weeks: a New Concept in Engineering Education", ICEE Hungary, New Challenges in Engineering Education and Research in the 21st Century, Budapest, August 2008.*

[Full Text][paper], [Slides][presentation]

Over the last six years, the Department of Electronics-ICT at the Faculty of Applied Engineering at
University College of Antwerp has undergone drastic changes. An academic concept based on a specific
profile for applied engineering education was used to develop an educational program in close liaison with
local industry. This concept is based upon progressive building and training of research skills. The concept
of thematic weeks was introduced to obtain a very good coupling between current research and education.
A typical thematic week combines theory, exercises, lab-sessions and a visit to an industrial partner. Speakers
from industry and academia are invited to provide a broad and accurate view of the topic in hand. During
each week, the students work on a single project. The Master's students are actively involved in organising
the thematic weeks. This has two positive consequences: development of managerial skills and very intensive
student involvement. The paper includes several case studies, presenting a selection of the topics from the
academic year 2007-2008.

Index terms: Thematic weeks, current research, week-long project, managerial skills, student involvement.

[Conference website][website], [iNEER][ineer]

[paper]: /static/articles/2008/icee2008.pdf
[presentation]: /static/articles/2008/icee2008pres.pdf
[website]: http://www.ineer.org/Events/ICEE2008Info/Welcome.htm
[ineer]: http://www.ineer.org/Welcome.htm
