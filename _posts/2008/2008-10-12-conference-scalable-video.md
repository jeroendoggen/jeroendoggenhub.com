---
layout: post
title: " Transmitting Scalable Video with UEP over 802.11b/g"
categories:
- conference
tags: [802.11b/g, RF propagation, H.264/SVC, mobility, SWiNWiMob]
---

*D'haenens R., Doggen J., Bakker D., Dams T., "Transmitting Scalable Video with Unequal Error Protection over 802.11b/g", IEEE International Workshop on Simulators for Wireless Networks, Avignon, October 2008.*

[Full Text][paper], [Slides][presentation], [Google Scholar][scholar]

We developed a simulation set-up that can test the behaviour of streaming applications over an error-prone wireless networks: 802.11b/g specifically. 
The application we tested is a video coder that adds unequal error protection to the scalable extension of H.264/SVC. 
This protection mechanism enables the recovery of lost packets. 
We adapted a radio wave propagation model to define Rayleigh fading which is able to model packet reception correctly for indoor environments. 
We investigated how the video encoder creates RTP packets and we noticed a very small payload, resulting in a high protocol overhead of 54%. 
We recommended the encoded designers to use UDP lite because it does not discard corrupt packets, as UDP does, but allows them to pass to the video decoder. 
This ensures more incoming information that can be evaluated at the application layer. 
The simulation of the 802.11b/g networks with bad wireless network conditions proves the correcting capabilities of the video decoder. 
The decoder could repair lost packets, or ensure graceful degradation. 
However this graceful degradation could not be maintained when the packet losses become too high, as for example when moving out of reach of an access point.

Index Terms: 802.11b/g, RF propagation, H.264/SVC, mobility, SWiNWiMob.

[Conference program][program], [Conference website][website]

[paper]: /static/articles/2008/wimob2008.pdf
[presentation]: /static/articles/2008/wimob2008pres.pdf
[website]: http://www.icsd.aegean.gr/SecPri_WiMob_2008/default.htm
[program]: http://www.informatik.uni-trier.de/~ley/db/conf/wimob/wimob2008.html
[scholar]: http://scholar.google.be/citations?view_op=view_citation&hl=en&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:u5HHmVD_uO8C
