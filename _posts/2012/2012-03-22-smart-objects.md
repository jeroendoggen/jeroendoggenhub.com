---
layout: post
title: " Smart Objects for Human Computer Interaction"
categories:
- conference
tags: [Smart Objects, Arduino, Human Computer Interaction, Jef Neefs, Enzo Brands]
---

*Doggen J., Neefs J., Brands E., Peeters T., Bracke J., Smets M., Van der Schueren F.: "Smart Objects for Human Computer Interaction, Experimental Study," ECUMICT, European Conference on the Use of Modern Information and Communication Technologies, Ghent, March 2012*

[Full Text][paper], [Slides][presentation], [Website][website], [Google Scholar][scholar]

The fields of embedded systems and networking are being merged, giving rise to the "Internet Of Things", built with smart objects that allow people to interact with everyday objects and vice-versa. 
We developed a cube, equipped with an Arduino based development board, various sensors and an XBee wireless interface, which enables us to develop new methods of human computer interaction. 
The cube was tested as an input controller for several computer games. 
The software was released under an open source license. 
This system will be used to develop new sensor driven applications.

Keywords: Smart Objects, Human Computer Interaction, Motion Sensing, Distance Sensing, Arduino.

[paper]: /static/articles/2012/ecumict2012.pdf
[presentation]: /static/articles/2012/ecumict2012pres.pdf
[website]: http://www.ecumict.be
[scholar]: http://scholar.google.be/citations?view_op=view_citation&hl=en&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:Y0pCki6q_DkC