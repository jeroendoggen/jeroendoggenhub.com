---
layout: post
title: " Arduino-based WSN Protocol for Smart Parking"
categories:
- conference
tags: [WSN, Wireless Sensor Networks, LEACH, Carlos, Luis]
---

 *Ostiz L., Pita C., Doggen J., Dams T., Van Houtven P.: "Wireless Sensor Network Protocol for Smart Parking Application, Experimental Study on the Arduino Platform" AMBIENT 2012, The Second International Conference on Ambient Computing, Applications, Services and Technologies Barcelona, September 2012.*

[Full Text][paper], [Slides][presentation], [Website][website], [Google Scholar][scholar]

The paper presents an Arduino-based wireless sensor network to monitor parking lots using a non-standard low-power energy-balanced system. 
The event-driven routing protocol follows the hierarchical clustering philosophy. 
Energy is saved by minimising the number of transmissions needed to forward information to the base station. 
The smart sensor platform is build using the popular Arduino development platform, Sharp IR distance sensors and nRF24 low-power radio modules. 
Our practical results show that this platform is easy to use, but not the most appropriate platform to develop low-power wireless sensor network applications.

[paper]: /static/articles/2012/ambient2012.pdf
[presentation]: /static/articles/2012/ambient2012pres.pdf
[website]: http://www.iaria.org/conferences2012/AMBIENT12.html
[scholar]: http://scholar.google.com.br/citations?view_op=view_citation&hl=nl&user=Co_vL8AAAAAJ&citation_for_view=Co_vL8AAAAAJ:UeHWp8X0CEIC