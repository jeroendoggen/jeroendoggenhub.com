---
layout: post
title: " Arduino Class: Tutorials, Articles and Code Samples"
categories:
- classes
tags: [Arduino, class, code, samples]
---

[Github page][github]

# Arduino class

Tutorials, articles and code samples for an embedded systems class using the Arduino platform

## Current topics:
### Getting Started
Arduino Tutorials, Good Practice Guidelines

### Intermediate
Object Oriented Embedded Programming, Classes Within Classes, Linking Nested Libraries, Casting (c vs c++ vs c#), Coding Style, Alternative Build Methods, Coding Standards

### Advanced
Protothreads, Memory Allocation, Static Code Analysis, Debugging AVR Dynamic Memory Allocation

##  Hardware
ATMega328P Datasheet, 8-Bit AVR Instruction Set (pdf), 10 Ways to Destroy an Arduino, Arduino Shields, Fritzing, Interfacing with Hardware


[github]: https://github.com/jeroendoggen/Arduino-class
