---
layout: post
title: " Analog Honeywell Humidity Sensors"
categories:
- arduino
tags: [Arduino, ArduMoto, motor, library]
---

[Github page][github], [SparkFun page][sparkfun]

# Arduino Library for Analog Honeywell Humidity Sensors

The library provides simplifies the usage of this sensor in Arduino projects.

The sensor is preconfigured for use at room temperature, but can be reconfigured at runtime.

## Library Usage
 * Download the source
 * Place the HIH4030Humidity folder in your Arduino1.0+ "libraries" folder
 * Open example sketch: "file", "Examples", "HIH4030Humidity", "HumidityPercentage" (or "RawData")
 * Connect the analog sensor to port A0 (and connect Vcc and GND)
 * Compile & upload code
 * Sensor data should be arriving over the serial port

## Screenshot:
![Serial monitor screenshot](https://raw.github.com/jeroendoggen/arduino-humidity-sensor-library/master/Images/screenshot.png?raw=true)

[github]: https://github.com/jeroendoggen/arduino-ardumotor-library
[sparkfun]: https://www.sparkfun.com/products/9815