---
layout: post
title: " Bash script: fix indentation of C++ code"
categories:
- Bash
tags: [Bash,indentation,c++]
---

This small scripts allows me fix the indentation of C++ source files with very little effort.

Usage:

 1. Drop one or more C++ source files in the "toindent" folder
 2. Run the script "./indenter.sh toindent" (this calls the bcpp indenter to indent all files in the 'toindent' folder)
 3. Done

There are some options for step 2:
 * Select one file to indent: "indent.sh file.cpp"
 * Select all files in the "toindent" folder: "./indent.sh toindent/"
 * Select only files with a certain extension in a folder (e.g. ".h"):  "./indent.sh toindent h"

Code available: [Github](https://github.com/jeroendoggen/scripts-tools-misc/tree/master/Indenter)
