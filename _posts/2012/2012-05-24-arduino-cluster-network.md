---
layout: post
title: " nRF24-based Cluster Networking"
categories:
- arduino
tags: [Arduino, nRF24, cluster networking, WSN]
---

[Github page][github]

# Arduino Cluster Networking

Arduino library for low-power wireless communication using nRF24 radio modules and LEACH based clustering network layer.

This code was developed in the spring semester of 2012 by Luis Ostiz Urdiain and Carlos Pita Romera during their Erasmus internship at Artesis University College Antwerp, Belgium.

## More info:

Coming soon (when I find the time to fix these issues...)

 * [Add the last version to Github][github-issue1]
 * [Test & debug the code][github-issue2] 

[github]: https://github.com/jeroendoggen/Arduino-cluster-networking
[github-issue1]: https://github.com/jeroendoggen/Arduino-cluster-networking/issues/1
[github-issue2]: https://github.com/jeroendoggen/Arduino-cluster-networking/issues/2