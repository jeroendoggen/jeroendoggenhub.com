---
layout: post
title: " Kate/KDevelop Syntax Highlighting Style"
categories:
- arduino
tags: [Arduino, KDE, Kate]
---

[Github page][github]

# Kate/KDevelop syntax highlighting style for Arduino code

## Usage:
 * Download and copy "ArduinoKate.xml" to $HOME/.kde/share/apps/katepart/syntax/
 * (re)start Kate
 * Open a .pde or .ino file in Kate
 * Syntax highlighting should be enabled automatically

## Tweaking:
To highlight your own libraries change the following lines in the .xml file
 * `<item>` addUserClassesHere `</item>`
 * `<item>` addUserMethodsHere `<item>`
 * `<item>` addUserObjectsHere `<item>`

## Screenshot:
![Screenshot](Images/screenshot.png?raw=true)

## Usage:
 * Download and copy "ArduinoKate.xml" to $HOME/.kde/share/apps/katepart/syntax/
 * (re)start Kate
 * Open a .pde or .ino file in Kate
 * Syntax highlighting should be enabled automatically
![Serial monitor screenshot](https://raw.github.com/jeroendoggen/Arduino-KDE-syntax-highlighting/master/Images/screenshot.png?raw=true)

[github]: https://github.com/jeroendoggen/Arduino-KDE-syntax-highlighting