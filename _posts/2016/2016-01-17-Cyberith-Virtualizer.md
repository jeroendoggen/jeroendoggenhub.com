---
layout: post
title: " Cyberith Virtualizer"
categories:
- embedded
tags: [embedded systems, Cyberith, Virtualizer, Vienna]
---

I worked in Vienna on the Virtualizer project for at the Austrian company [Cyberith GmbH][cyberith], from mid 2015 to end 2016.

My main tasks:
 * Firware development: sensor interfacing, communication, build automation (STM32, C, Jenkins)
 * Development of the Virtualizer SDK (C++ on Windows)
 * Development of semi-automated hardware verification tests (Firmware in C and tests in Python on Linux)
 * Field engineer: installation of Virtualizer setups

[cyberith] http://cyberith.com/
