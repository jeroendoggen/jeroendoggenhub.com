---
layout: default
title: About - biography
---

## Short biography:
Jeroen Doggen is an embedded systems engineer at the Austrian company CDE GmbH
He currently designs hard- and software for biomedical devices.
He developed parts of the Virtualizer, a locomotion device for virtual reality at Cyberith GmbH in Vienna, Austria.
He is the author of several open source embedded software libraries for the Arduino platform: sensor interfacing, motor control, unit testing, wireless communication.
He acted as a reviewer for the following conferences and magazines: AMBIENT, ECUMICT, IEEE Communications Letters. 
He has given several international guest lectures at partner Universites in Finland and Austria, during his time as a lecturer and researcher at both the Universiy of Anwerp and Artesis Plantijn University College Antwerp.
He has taught various courses in the fields of digital design, embedded systems and wireless sensor networks at Artesis Plantijn University College Antwerp. 
More information about Jeroen can be found on his personal website: http://jeroendoggen.github.io
