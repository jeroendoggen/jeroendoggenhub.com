This repository contains the Jekyll source for my [personal website][mysite].

![](https://raw.github.com/jeroendoggen/jeroendoggen.github.com/master/static/images/2013/blog2.png)

Spelling/grammar corrections are always welcome (and much appreciated :heart:) via Pull Requests.

---
### Theme
The theme is [Lagom][lagom], tweaked to my own liking. 

---

### Licenses
The following directories and their contents are Copyright Jeroen Doggen. You may not reuse anything therein without my permission:

* _posts/
* talks/
* static/

All other directories and files are MIT Licensed. Font Awesome is licensed under SIL OFL 1.1 and MIT Licences and I claim no ownership.

Licensing for the [Lagom][lagom] theme is MIT.

[mysite]: http://jeroendoggen.github.io/
[lagom]: http://jekyllthemes.org/themes/lagom/
